const webpack = require('webpack');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  plugins: [
    new webpack.DefinePlugin({'process.env.NODE_ENV': JSON.stringify('development') }),
    new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
        jquery: 'jquery'
    }),
  ],
  devServer: {
    port: 9000,
    hot: true,
    inline: true,
    open: true,
    index: 'index.html',
  },
});