# React-App Grundgerüst
last Update 04.2021
## Installation

1. Kommandozeile öffnen und zu gewünschten Ordner navigieren
2. Code von repository in den Ordner klonen:
    `git clone https://bitbucket.org/betriebmachen/boilerplate-react.git`
3. Bibs installieren
    `npm install`
4. Packen und Server starten
    `npm run dev`
5. Im Browser unter __http://localhost:9000__ ist App erreichbar. Bei Änderungen am Code, refresht die Seite automatisch (hot loading)

## Wartung

Pakete regelmaßig über npm updaten (__ncu__ ist zu empfehlen).

`ncu -u`

`npm install`

## Start

1. `npm run dev` - Packt mit webpack und startet den lokalen Testserver http://localhost:9000/
2. `npm run package` - Packt mit Webpack im Produktionsmodus. Unter /dist werden alle fertigen Dateien abgelegt.

## Integration als neues Projekt

1. Projekt normal wie oben installieren und neues git online anlegen (leeres Repository)
2. **Git-Assistent** (*_git.bat*) benutzen um Repository zu löschen und neues anzulegen
3. `_git cut` - Löschen des Repositorys
4. `_git add https://...` - Hinzufügen des neuen Repository
5. `_git push "Updatenachricht"` - Upload der aktuellen Version

## GIT Updates usw

_Lade quellcode hoch:_
`_git push "*Nachricht*"`

_Füge Repository diesem Projekt zu:_
`_git add *url*`

_Entferne akutelles Repository aus diesem Projekt:_
`_git cut`

### Struktur

`/src` - Alle Quellcodedateien

`/dist` - Fertiges gepacktes Projekt von webpack zum Verteilen.

`/vendor` - Extern heruntergeladene Dateien, die nicht über npm zu finden sind.

`.gitignore` - Liste mit Ordnern und Dateien die für den Upload (git) ausgeschlossen werden sollen.

`webpack.common.js` - Gemeinsame Konfiguration von webpack für Dev und Produktions-Modus.

`webpack.*.js` - Spezielle Konfiguration für den jeweiligen Modus von Webpack.

`/src/index.html` - Einstiegspunkt vom Server

`/src/index.js` - Einstiegspunkt von JS

`/src/js/components` - Speicherort für alle übergreifenden React-Komponenten

`/src/js/layouts` - Speicherort für alle Layouts der React-Seiten

`/src/js/pages` - Speicherort für alle Seiten innerhalb der React-App, die über Routes dynamisch geladen werden

`/src/js/stores` - Speicherort für alle Daten-Stores für die React-App (siehe mobx)

`/src/scss` - Speicherort für alle CSS/SCSS Dateien.

`/src/scss/pages` - Speicherort für alle SCSS Dateien für die React-Seiten (für js/pages/)

`/src/scss/components` - Speicherort für alle SCSS Dateien für die React-Komponenten (für js/components/)

`/src/scss/layouts` - Speicherort für alle SCSS Dateien für die React-Layouts (für js/layouts/)

`/src/scss/App.global.scss` - Einsprungpunkt für CSS

`/src/scss/CustomStyle.scss` - Farben, Größen, etc für die Stylevorgabe von BetriebMachen

`/src/scss/Rajdhani.scss` - Schriftart Rajdhani

`/src/fonts/..` - Speicherort für die Schriftarten

### Verwendete Frameworks/Tools
#### SCSS / CSS

  Für die CSS-Styles wird `node-sass` verwendet.

  Es gibt __zwei Arten__, wie CSS in die App integriert werden kann:

  Entweder als __*dateiname*.global.scss__:
  Dann werden die dort definierten Klassen global in der ganzen App verfügbar und die Klassennamen bleiben gleich. Eine hier definierte Klasse kann von allen Komponenten verwendet werden.

  Oder als __*dateiname*.scss__:
  Dann wird die CSS datei und deren Klassennamen gehasht und damit einzigartig, für die Komponente, die die SCSS importiert hat (Vorteil bei doppelten Klassennamen).
