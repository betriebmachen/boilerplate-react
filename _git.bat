@echo off

ECHO -------------------------
ECHO Git Assistent gestartet.
ECHO -------------------------

IF "%1"=="help" ( GOT help )
IF "%1"=="push" ( GOTO push )
IF "%1"=="add" (GOTO add)
IF "%1"=="cut" (GOTO cut )
IF "%1"=="browser" ( GOTO browser )

GOTO help

:help
	ECHO Führe diese Befehle aus
  ECHO "Lade Quellcode hoch:"
  ECHO _git push "*Nachricht*"
  ECHO ----------------------
  ECHO "Fuege Repository diesem Projekt zu:"
  ECHO _git add *url*
  ECHO ----------------------
  ECHO "Entferne akutelles Repository aus diesem Projekt:"
  ECHO _git cut
  ECHO ----------------------

GOTO End1

:push
	ECHO lade Quellcode zum Git-Repository hoch...
	git add *
  git commit -m %2
  git push
GOTO End1

:cut
  ECHO Boilerplate Verbindung zum git entfernen
  git remote rm origin
  ECHO Neue Verbindung (origin) hinzufügen
  git remote add origin https...git
  ECHO Neu konfigurieren
  git config master.remote origin
  ECHO Zusammenführen
  git config master.merge refs/heads/master
  ECHO Code zum neuen Origin hochladen
  git push -u origin master

GOTO End1

:add
  ECHO Füge %2 als Repository diesem Projekt hinzu
  git remote add origin %2
:End1

ECHO -------------------------
ECHO Git Assistent beendet.
ECHO -------------------------
