const webpack = require('webpack');
const path = require('path');

const SRC_DIR = path.resolve(__dirname, 'src');
const OUTPUT_DIR = path.resolve(__dirname, 'dist');

module.exports = {

  entry: SRC_DIR+"/index.js",
  output: {
    path: OUTPUT_DIR,
    filename: 'index.js'
  },
  module: {
    rules: [
      // JS
      {
        test: /\.jsx?$/,
        include : SRC_DIR,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
                  presets: ['@babel/preset-env'],
                  plugins: ['transform-class-properties']
              }
          },
        ],
      },
      // scss dateien mit "global" werden direkt gepackt
      {
        test: /\.global\.scss$/,
        use: [
          { loader: 'style-loader' },
          {
            loader: 'css-loader',
            options: { sourceMap: false },
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: false }
          }
        ]
      },
      // scss dateien OHNE "global" werden als hash gepackt
      {
        test: /^((?!\.global).)*\.scss$/,
        use: [
          { loader: 'style-loader',},
          { loader: 'css-loader',
            options: {
              modules: {
                  localIdentName: "[name]__[local]___[hash:base64:5]",
              },
              sourceMap: false,
              importLoaders: 1,
            }
          },
          { 
            loader: 'sass-loader',
            options: { sourceMap: false }
          }
        ]
      },
      // CSS
      {
          test: /\.css$/,
          use: [
            { loader: 'style-loader' },
            {
              loader: 'css-loader',
              options: { sourceMap: false },
            }],
      },
      {
          test: /\.html$/,
          use: [
            {
              loader: "file-loader" ,
              options: {
                name: '[name].[ext]',
              }
            }
          ],
      },
      {
         test: /.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
         use: [{
           loader: 'file-loader',
           options: {
             name: '[name].[ext]',
             publicPath: '/fonts',
             outputPath: '/fonts'
           }
         }]
       },
       {
          test: /.(png|jpg|gif?)(\?[a-z0-9]+)?$/,
          use: [{
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              publicPath: '/img',
              outputPath: '/img'
            }
          }]
        }
    ]
  },
  devServer: {
    contentBase: [OUTPUT_DIR],
  }
};
