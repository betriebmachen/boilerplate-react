import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

export default class Main extends React.Component {

  constructor(props) {
      super(props);
  }

  render() {
    return <div className="uk-container uk-container-large">{this.props.children}</div>
  }
}
