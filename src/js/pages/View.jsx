import React from 'react'
import { Link } from 'react-router-dom'

import css from './../../scss/pages/View.scss'

export default class View extends React.Component {

    render() {
      return (
                <div>
                  <h1>Page: View</h1>
                  <p>Du bist auf {this.props.location.pathname}</p>
                  <Link to="/Home"><div>Home</div></Link>
                </div>
          )

    }
}
