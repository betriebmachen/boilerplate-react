import React from 'react'
import { Link } from 'react-router-dom'

// css
import css from './../../scss/pages/Index.scss'

export default class Index extends React.Component {

    render() {
      return (
              <div>
                  <h1 className="uk-heading-small">Page: Index</h1>
                  <p>Du bist auf: {this.props.location.pathname}</p>
                  <Link to="/View"><button className="uk-button uk-button-primary">View</button></Link>
              </div>
          )

    }
}
