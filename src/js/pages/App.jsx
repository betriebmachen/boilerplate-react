import React from 'react'
import { HashRouter as Router, Route, Switch, IndexRoute, hashHistory } from 'react-router-dom'

import MainLayout from './../layouts/Main.jsx'

import IndexPage from './Index.jsx'
import ViewPage from './View.jsx'

export default class App extends React.Component {

    render() {
      // routing
      return (
              <Router>
                  <MainLayout>
                    <Switch>
                      <Route exact path='/' component={IndexPage} />
                      <Route path="/Home" component={IndexPage}></Route>
                      <Route path="/View" component={ViewPage}></Route>
                    </Switch>
                  </MainLayout>
              </Router>
          )

    }
}
