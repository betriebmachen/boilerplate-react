import React from 'react'

export class Container extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array("container");

      // disable spacing
      (this.props.gapless) ? classes.push(`col-gapless`) : null;

      // maxwidth of container
      // == xs,sm,md,lg,xl
      (this.props.maxWidth) ? classes.push(`grid-${this.props.maxWidth}`) : null;

      // verstecken bis zu gegebener größe, bei nächster größe wird angezeigt
      // == xs,sm,md,lg,xl
      (this.props.hide) ? classes.push(`hide-${this.props.hide}`) : null;

      // zeigt container bis zu gegebener größe, bei nächster größe wird nicht angezeigt
      // == xs,sm,md,lg,xl
      (this.props.show) ? classes.push(`show-${this.props.hide}`) : null;

      (this.props.className)   ? classes.push(`${this.props.className}`) : null ;

      return (
        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            {this.props.children}
        </div>
        );
    }
}

export class Row extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array("columns");

      // disable spacing
      (this.props.gapless) ? classes.push(`col-gapless`) : null;
      // alle cols auf eine zeile
      (this.props.oneline) ? classes.push(`col-oneline`) : null;

      (this.props.className)   ? classes.push(`${this.props.className}`) : null ;

      return (
        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            {this.props.children}
        </div>
        );
    }
}

export class Col extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array('column');

      (this.props.col)  ? classes.push(`col-${this.props.col}`)   : null ;
      (this.props.xs)   ? classes.push(`col-xs-${this.props.xs}`)   : null ;
      (this.props.sm)   ? classes.push(`col-sm-${this.props.sm}`) : null ;
      (this.props.md)   ? classes.push(`col-md-${this.props.md}`) : null ;
      (this.props.lg)   ? classes.push(`col-lg-${this.props.lg}`) : null ;
      (this.props.xl)   ? classes.push(`col-xl-${this.props.xl}`) : null ;

      // vertikale ausrichtung
      (this.props.offset == "left")  ? classes.push(`col-ml-auto`) : null ;
      (this.props.offset == "right") ? classes.push(`col-mr-auto`) : null ;
      (this.props.offset == "center")? classes.push(`col-mx-auto`) : null ;

      // Auto width
      (this.props.auto )? classes.push(`col-auto`) : null ;

      // Auto width, size
      (this.props.xsAuto)   ? classes.push(`col-xs-auto`) : null ;
      (this.props.smAuto)   ? classes.push(`col-sm-auto`) : null ;
      (this.props.mdAuto)   ? classes.push(`col-md-auto`) : null ;
      (this.props.lgAuto)   ? classes.push(`col-lg-auto`) : null ;
      (this.props.xlAuto)   ? classes.push(`col-xl-auto`) : null ;


      (this.props.className)   ? classes.push(`${this.props.className}`) : null ;

      return (
        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            {this.props.children}
        </div>
        );
    }
}

export class Button extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array("btn");

      // color
      (this.props.primary) ? classes.push(`btn-primary`) : null;
      (this.props.link) ? classes.push(`btn-link`) : null;
      (this.props.success) ? classes.push(`btn-success`) : null;
      (this.props.error) ? classes.push(`btn-error`) : null;
      // size
      (this.props.lg) ? classes.push(`btn-lg`) : null;
      (this.props.sm) ? classes.push(`btn-sm`) : null;
      (this.props.block) ? classes.push(`btn-block`) : null;

      //shape
      (this.props.action) ? classes.push(`btn-action`) : null;
      (this.props.circle) ? classes.push(`s-circle`) : null;

      // state
      (this.props.active) ? classes.push(`active`) : null;
      (this.props.disabled) ? classes.push(`disabled`) : null;
      (this.props.loading) ? classes.push(`loading`) : null;

      (this.props.className)   ? classes.push(`${this.props.className}`) : null ;

      return (
        <button
          type="button"
          className={classes.join(" ")}
          disabled={(this.props.disabled) ? true : false}
          role="button"
          style={this.props.style}
          onClick={this.props.onClick}
          id={(this.props.id)? this.props.id : ""}
          data-toggle={(this.props.dataToggle)? this.props.dataToggle : ""}>

            {this.props.children}

        </button>
        );
    }
}

export class Icon extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array('icon');

      // icon
      // https://picturepan2.github.io/spectre/elements/icons.html
      (this.props.name )? classes.push(`icon-${this.props.name}`) : null ;

      // size
      // icon-2x, icon-3x and icon-4x
      (this.props.size )   ? classes.push(`icon-${this.props.size}x`) : null ;

      (this.props.className)  ? classes.push(`${this.props.className}`) : null ;

      // farbe
      let color = (this.props.color) ? {color: this.props.color} : {};

      let style = (this.props.style) ? Object.assign(this.props.style,color) : Object.assign({},color);

      return (
        <i className={classes.join(" ")} style={style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            {this.props.children}
        </i>
        );
    }
}

export class Card extends React.Component {

    /*
    <Card>
      <CardHeader title="Beispielkarte" subtitle="Das ist eine Beispielkarte" />
      <CardImage src="img/bsp.jpg" />
      <CardBody>
        <p>Beispieltext und so Lorem ipsum</p>
      </CardBody>
      <CardFooter>
        Ende der Karte mit Buttons oder so
      </CardFooter>
    <Card>

    */

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array('card');

      (this.props.className)  ? classes.push(`${this.props.className}`) : null ;

      return (

        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            {this.props.children}
        </div>
        );
    }
}

export class CardImage extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array('card-image');

      (this.props.className)  ? classes.push(`${this.props.className}`) : null ;

      return (

        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            <img src={this.props.src} className="img-responsive" />
        </div>
        );
    }
}

export class CardBody extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array('card-body');

      (this.props.className)  ? classes.push(`${this.props.className}`) : null ;

      return (

        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            {this.props.children}
        </div>
        );
    }
}

export class CardFooter extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array('card-footer');

      (this.props.className)  ? classes.push(`${this.props.className}`) : null ;

      return (

        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
            {this.props.children}
        </div>
        );
    }
}

export class CardHeader extends React.Component {

    constructor(props){
      super(props);
    }

    render() {

      var classes = Array('card-header');

      (this.props.className)  ? classes.push(`${this.props.className}`) : null ;

      return (

        <div className={classes.join(" ")} style={this.props.style}
          onClick={this.props.onClick}
          onContextMenu = {this.props.onContextMenu}
          onDoubleClick = {this.props.onDoubleClick}
          onDrag = {this.props.onDrag}
          onDragEnd = {this.props.onDragEnd}
          onDragEnter = {this.props.onDragEnter}
          onDragExit = {this.props.onDragExit}
          onDragLeave = {this.props.onDragLeave}
          onDragOver = {this.props.onDragOver}
          onDragStart = {this.props.onDragStart}
          onDrop = {this.props.onDrop}
          onMouseDown = {this.props.onMouseDown}
          onMouseEnter = {this.props.onMouseEnter}
          onMouseLeave = {this.props.onMouseLeave}
          onMouseMove = {this.props.onMouseMove}
          onMouseOut = {this.props.onMouseOut}
          onMouseOver = {this.props.onMouseOver}
          onMouseUp = {this.props.onMouseUp}
          onTouchCancel = {this.props.onTouchCancel}
          onTouchEnd = {this.props.onTouchEnd}
          onTouchMove = {this.props.onTouchMove}
          onTouchStart = {this.props.onTouchStart}
          >
          <div className="card-title h5">{this.props.title}</div>
          <div className="card-subtitle text-gray">{this.props.subtitle}</div>
        </div>
        );
    }
}

export class SideBar extends React.Component {

  constructor(props) {
      super(props);
      this.state = {
        sidebarActive: false
      }
  }

  menuClicked() {
    this.setState({sidebarActive: !this.state.sidebarActive});
    return false;
  }

  render() {

    let active = (this.state.sidebarActive) ? 'active' : false
    let menu = (this.props.menu) ? this.props.menu : 'this.props.menu'
    let content = (this.props.content) ? this.props.content : 'this.props.content'

    return (

      <div className="off-canvas off-canvas-sidebar-show">

        <a className="off-canvas-toggle btn btn-primary btn-action" onClick={this.menuClicked.bind(this)}>
          <i className="icon icon-menu"></i>
        </a>

        <div id="sidebar-id" className={`off-canvas-sidebar ${active}`}>
          {menu}
        </div>

        <a className="off-canvas-overlay" onClick={this.menuClicked.bind(this)}></a>

        <div className="off-canvas-content">
          {content}
        </div>
      </div>

        )

  }
}
