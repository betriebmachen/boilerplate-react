// react laden
import React from 'react'
import ReactDOM from 'react-dom'

// lade index html, damit webpack die datei in /dist kopiert
import Index from './index.html'

// lade standard bibliotheken

// lade das globale css file
import AppCSS from './scss/App.global.scss';

// importiere Haupt App Komponente.
import App from './js/pages/App.jsx'

// lade App in den DOM (in container mit id="app")
ReactDOM.render( <App />, document.getElementById('app'));
